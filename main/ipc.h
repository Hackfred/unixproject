#include <stdio.h>
#include <stdlib.h>
#include <sys/msg.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <semaphore.h>


#define MSG_QUEUE_MAX_LENGTH 100
#define FIFO_MAX_LENGTH 		 100
#define SHM_SIZE 						 300


//msg queue
struct msg_queue_buf {
	long mtype;
	char mtext[MSG_QUEUE_MAX_LENGTH];
};

extern key_t ipc_key;
extern int msg_queue_id;

extern void msg_queue_prepare();

extern void msg_queue_setup_queue();
extern void msg_queue_get_queue();

extern void msg_queue_send(char * buf);
extern char * msg_queue_recieve();

//fifo
extern int fifo_pd;

extern void fifo_mk();
extern void fifo_open_for_read();
extern void fifo_open_for_write();

extern void fifo_write(char * buf);
extern char * fifo_read();

extern void fifo_close();

//shared memory
extern int shm_id;
extern char * shm_buffer;


extern void shm_create();
extern void shm_connect();
extern void shm_write(char * s);
extern char * shm_read();
extern void shm_disconnect();
extern void shm_delete();


//semaphores
struct slock {
	sem_t *semp;
	char name[100];
	int exists;
};

extern struct slock * sema_lock;

extern void sema_create();
extern void sema_connect();
extern void sema_free();
extern void sema_locki();
extern void sema_trylock();
extern void sema_unlock();

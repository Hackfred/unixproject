#include "ipc.h"
#include "errors.h"
#include <errno.h>

void msg_queue_prepare() {
	char * shared_string = "asdfasdf";
	ipc_key = ftok(shared_string, 1);
}

void msg_queue_setup_queue() {
	msg_queue_id = msgget(ipc_key, IPC_CREAT|0644);
	//change this later to using err()
	if (msg_queue_id == -1) {
		err(1, "could not create msg queue");
	}
}

void msg_queue_get_queue() {
	//change this later to using err()
	if ((msg_queue_id = msgget(ipc_key, 0)) < 0) {
		err(1, "could not connect to msg queue");
	}
}

void msg_queue_send(char * buf) {
	int len = strlen(buf);
	struct msg_queue_buf msg1;
	
	msg1.mtype = 1;
	strcpy(msg1.mtext, buf);

	if (msgsnd(msg_queue_id, (void *) &msg1, MSG_QUEUE_MAX_LENGTH, IPC_NOWAIT) == -1) {
		err(1, "error while trying to send msg trough msg queue");
	}
}

char * msg_queue_recieve() {
	struct msg_queue_buf inmsg;
	int len;
	char * buf;
	
	len = msgrcv(msg_queue_id, &inmsg, MSG_QUEUE_MAX_LENGTH, 0, 0);
	buf = malloc(sizeof(char *) * (len +1));
	strcpy(buf, inmsg.mtext);

	return buf;
}


//fifo
void fifo_mk() {
	char * fifo_path = "./FIFO";

	if (access(fifo_path, F_OK) != -1) {
		//fifo already exists, so it will be deleted
		if (unlink(fifo_path) == -1) {
			err(1, "could not delete existing fifo");
		}
	}
	if (mkfifo(fifo_path, 0666) == -1) {
		err(1, "error while creating fifo");
	}
}

void fifo_open_for_read() {
	if ((fifo_pd = open("./FIFO", O_RDONLY | O_NONBLOCK)) == -1) {
		err(1, "could not open fifo for reading");
	}
}

void fifo_open_for_write() {
	if ((fifo_pd = open("../main/FIFO", O_WRONLY | O_NONBLOCK)) == -1) {
		err(1, "could not open fifo for writing");
	}
}

void fifo_write(char * buf) {
	int n;
	if ((n = write(fifo_pd, buf, FIFO_MAX_LENGTH)) == -1) {
		err(1, "error while writing to fifo");
	}
}

char * fifo_read() {
	int n, all = 0;
	char buf[FIFO_MAX_LENGTH];
	char * r = NULL;
	
	while ((n = read(fifo_pd, buf, FIFO_MAX_LENGTH)) > 0) {
		all += n;
		if (r == NULL) {
			r = malloc(sizeof(char *) * n);
		}	else {
			r = realloc(r, sizeof(char *) * all);
		}
		strcpy(r, buf);
	}
	
	if (n == 0) {
		return NULL;
	}

	if (n == -1 && errno != EAGAIN) {
		err(1, "error while reading from fifo");
	} 
	
	return r;
}

void fifo_close() {
	close(fifo_pd);
}

//shared memory
void shm_create() {
	char shm_id_s[SHM_SIZE];

	//create the global sema first
	sema_create();

	shm_id = shmget(IPC_PRIVATE, SHM_SIZE * sizeof(char *), IPC_CREAT | 0666);
	if (shm_id == -1) {
		err(1, "could not create shared memory");
	}

	sprintf(shm_id_s, "%d", shm_id);

	if (setenv("shm_id", shm_id_s, 1) == -1) {
		err(1, "could not set environment variable shm_id");
	}
}

void shm_connect() {
	char * env_a;
	void * shm_data;
	
	if (shm_id == 0) {
		//the shm_id is not initialized, therefore check the environment variables
		if ((env_a = getenv("shm_id")) == NULL) {
			err(1, "could not get shm key from environment");
		}
		shm_id = atoi(env_a);

		//we knwo that we are the process, that has not created the semaphore, so it has to exists already
		sema_connect();
	}

	shm_data = shmat(shm_id, NULL, 0);
	if (shm_data == (void *) -1) {
		err(1, "could not connect to shared memory");
	}
	shm_buffer = (char *) shm_data;
}

void shm_write(char * s) {
	sema_locki();
	strcpy(shm_buffer, s);
	sema_unlock();
}

char * shm_read() {
	char * buf;
	buf = malloc(sizeof(char) * SHM_SIZE);

	sema_locki();
	strcpy(buf, shm_buffer);		
	sema_unlock();

	return buf;
}

void shm_disconnect() {
	if (shmdt(shm_buffer) == -1) {
		err(1, "could not disconnect from shared memory");
	}
}

void shm_delete() {
	if (shmctl(shm_id, IPC_RMID, NULL) == -1) {
		err(1, "could not delete shared memory");
	}
}

//semaphores for shared memory
void sema_create() {
	char * sem_name = "/sema1";
	sema_lock = malloc(sizeof(struct slock *));

	if (sema_lock->exists == 1) {
		//the semaphore already exists, so nothing has to be done
		return ;
	}

	sem_unlink(sem_name);	

	sema_lock->semp = sem_open(sem_name, O_CREAT|O_EXCL, 0666, 1);
	strcpy(sema_lock->name, sem_name);
	sema_lock->exists = 1;
	
	if (sema_lock->semp == SEM_FAILED) {
		free(sema_lock->semp);
		err(1, "could not open a POSIX semaphor");
	}

}

void sema_connect() {
	char * sem_name = "/sema1";
	sema_lock = malloc(sizeof(struct slock *));

	if ((sema_lock->semp = sem_open(sem_name, 0)) == SEM_FAILED) {
		err(1, "could not open existing semaphore");
	}
	strcpy(sema_lock->name, sem_name);
	sema_lock->exists = 1;
	

}

void sema_free() {
	sem_close(sema_lock->semp);
	sem_unlink(sema_lock->name);	
}

void sema_locki() {
	sem_wait(sema_lock->semp);
}

void sema_trylock() {
	sem_trywait(sema_lock->semp);
}

void sema_unlock() {
	sem_post(sema_lock->semp);
}

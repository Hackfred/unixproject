#include "errors.h"

void setup_sem_error() {
	sem_key = 0;
	sem_id_error = 0;
	
}

void write_err(char * s) {
	
	int fd, len;

	if (s == NULL) {
		return ;
	}
	
	len = strlen(s);

	if (sem_id_error == 0) {
		//sema has not been initialized
		sem_id_error = new_sem(1);
	}
	
	p(sem_id_error);
	
	//not nice to hardcode, should be changed
	fd = open("./logs/errorlog.txt", O_CREAT | O_WRONLY | O_APPEND, S_IRUSR | S_IWUSR | S_IXUSR);
	if ( write(fd, s, len) != len) {
		//handle errors here
	}
	close(fd);

	v(sem_id_error);
	
}

char * get_current_time_string() {

	FILE * fp;
 	char * times;

	int l = sizeof(char *) * 256;	

	times = malloc(l);

	fp = popen("date", "r");
	if (fp == NULL) {
		fprintf(stderr, "error getting the date");
	}
	if (fgets(times, l, fp) == NULL) {
		fprintf(stderr, "did not receive the date from pipe");
	}
	
	pclose(fp);
	
	return times;
}

void err(int code, char * msg) {
	int s = strlen(msg);
	char * tmp;
	if ((tmp = malloc(sizeof(char *) * (s+100))) == NULL) {
		perror("bad malloc error, during error handling");
		exit(-1);
	}

	strcpy(tmp, get_current_time_string());
	strcat(tmp, msg);
	strcat(tmp, "\n");

	fprintf(stderr, "%s\n", msg);

	//write error to errorlog
	write_err(tmp);
	sleep(1);

	switch (code) {
		case 0:
			//dont terminate any process, so just do nothing
			break;
		case 1: 
			//only terminate this process
			kill(getpid(), SIGQUIT);
			break;
		case 2:
			//terminate this process and its parent
			kill(getppid(), SIGQUIT);
			kill(getpid(), SIGQUIT);
			break;
		case 3:
			//terminate this process and its child
			if (current_child_pid != 0) {
				kill(current_child_pid, SIGQUIT);
			}
			kill(getpid(), SIGQUIT);
			break;
	}
}

int new_sem(int value) {
		
	int sem_id;

	union semun {
		int val;
		struct semid_ds *buf;
		ushort * array;
	} arg;

	arg.val = value;

	if (sem_key == 0) {
		sem_key = ftok(".", 12345678);
	}

	sem_id = semget(sem_key, 1, 0666 | IPC_CREAT);

	if (semctl(sem_id, 0, SETVAL, arg) == -1) {
		perror("sema could not be inited");
		//error handling, dont use err() here, LOOP!!!!
	}
	
	return sem_id;
}

void p(int sem_id) {
	
	struct sembuf ops[1];
	
	ops[0].sem_num = 0;
	ops[0].sem_op = -1;
	ops[0].sem_flg = 0;
	
	semop(sem_id, ops, 1);
}

void v(int sem_id) {
	
	struct sembuf ops[1];
	
	ops[0].sem_num = 0;
	ops[0].sem_op = 1;
	ops[0].sem_flg = 0;
	
	semop(sem_id, ops, 1);
}

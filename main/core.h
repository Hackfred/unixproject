#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/sem.h>
#include <string.h>
#include <pthread.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "ipc.h"
#include "errors.h"

#define FUNCTIONS_COUNT				5

extern pthread_mutex_t * mutex_thr;

extern char * get_cwd();
extern char * proc_args(int argc, char * argv[], int * toExecute);
extern void init_env(char ** env);
extern pid_t spawn_child(int n, char * path);
extern void handler(int signo);
extern void *thread_signals(void *arg);
extern void * thread_msg_queue(void * arg);
extern void write_stat_file(char * buf);
extern void manage_resources();

#include "core.h"

#define FUNCTIONS_COUNT 5

char * get_cwd() {
	long size;
	char *buf, *ptr;

	size = pathconf(".", _PC_PATH_MAX);
	
	if ((buf = malloc((size_t) size)) == NULL) {
		err(1, "could not get cwd, malloc error");
	}
	ptr = getcwd(buf, size);

	return ptr;
}

void write_stat_file(char * s) {
	int fd, l;
	
	if (s == NULL) {
		return ;
	}

	if ((fd = open("./logs/stats.txt", O_WRONLY | O_CREAT | O_APPEND, 0777)) < 0) {
		err(1, "could not open stat file");
	}

	l = strlen(s);

	if (write(fd, s, l) != l) {
		err(1, "error while writing to stat file");
	}

	close(fd);
}

char * proc_args(int argc, char * argv[], int toExecute[FUNCTIONS_COUNT+1]) {
	
	int i, a;	
	char * path = 0x0;
	char current_descriptor = '0';
	int toExecute_counter = 0;

	for (i = 1; i < argc; i++) {
		/*
		argument structure should be like ./cleaer -<desciptor> [value1, value2, ...]
	
		descriptor can be -a to specify the actions (e.g. "-a 1 2 3" to execute the first 3 processes)
		if not specified, all four processes are executed

		descriptor can be -p to specify the starting directory (e.g. "-p /home/xyz/")
		if not specified the current directoy is taken as a start
		*/
		if (argv[i][0] == '-') {
			current_descriptor = argv[i][1];
		} else {
			switch (current_descriptor) {
				case 'p':
					path = malloc(sizeof(char *) * strlen(argv[i]));
					strcpy(path, argv[i]);
					break;
				case 'a':
					if ((a = atoi(argv[i])) == 0) {
						err(3, "arguments should only consist of integers");
					} else if (a > FUNCTIONS_COUNT) {
						err(3, "arguments should be smaller than 5");
					} else {
						toExecute[toExecute_counter] = a;
						toExecute_counter++;
					}
					break;
				case '0':
					err(3, "arguments should always start with a descriptor");
					break;
				default:	
					err(3, "arguments contain unknown descriptor, use only -a and -p");
					break;		
			}
		}
	}

	if (path == 0x0) {
		path = get_cwd();
	}
	if (toExecute_counter == 0) {
		for (i = 0; i < FUNCTIONS_COUNT; i++) {
			toExecute[toExecute_counter] = (i+1);
			toExecute_counter++;
		}
	}
	
	//terminate the array with 0
	toExecute[toExecute_counter] = 0;

	return path;
}

void init_env(char ** env) {
	char s_ruid[25], s_euid[25];	

	uid_t ruid = getuid();
	uid_t euid = geteuid();
	gid_t rgid = getgid();
	gid_t egid = getegid();
	 
	sprintf(s_ruid, "ruid=%d", (int) ruid);
	sprintf(s_euid, "euid=%d", (int) euid);
	
	env[0] = s_ruid;
	env[1] = s_euid;	
	env[2] = NULL;
}

//creates a new process and passes the path information
pid_t spawn_child(int n, char * path) {
	
	pid_t p;
	char * list[] = {"name to display", path, NULL};
	char * env[10];	

	init_env(env);

	printf("\n--------------------------------------------------------------------\n");
	printf("Start of process #%d\n", n);
	printf("--------------------------------------------------------------------\n\n");

	//debug
	int num;
	
	switch (p = fork()) {
		case 0:
			switch (n) {
				case 1:
					if (execlp("../1/holeremover", "cleaner_holes", path, (char *)0) < 0) {
						err(3, "process could not be spawned, exec error");
					} 
					break;
				case 2:
					list[0] = "cleaner_information";
					if (execvp("../2/task2", list) < 0) {
						err(3, "process could not be spawned, exec error");
					} 
					break;
				case 3: 
					//integrate dions work
					list[0] = "cleaner_oldfiles";
					if (execv("../3/oldfiles", list) < 0) {
						err(3, "process could not be spawned, exec error");
					}
					break;
				case 4:
					list[0] = "cleaner_delete_links";
					if (execve("../4/delLink", list, env) < 0) {
						err(3, "process could not be spawned, exec error");
					}
					break;
				case 5:
					if (execl("../5/counter", "cleaner_counter", path, (char *)0) < 0) {
						err(3, "process could not be spawned, exec error");
					}
					break;
				default:
					//this should never happen
					err(1, "process number is invalid");
					break;	
			}
			break;
		case -1:
		 	err(1, "process could not be spawned, fork error");
			break;
		default:
			//wait until child finishes, sequentially
			waitpid(p, NULL, 0);
			printf("\n--------------------------------------------------------------------\n");
			printf("End of process #%d\n", n);
			printf("--------------------------------------------------------------------\n\n");
			break;
	}
	return p;
}

void handler(int signo) {
	printf("The program had to be shutdown. Check the logfile for more detailed information\n");
	psignal(signo, "Signal");
	exit(1);
}

//thread that listens for signals
void * thread_signals (void *arg) {
	
	void (* h)(int);

	h = signal(SIGQUIT, handler);
	if (h == SIG_ERR) {
		perror("could not register signal handler.");
		exit(1);
	}

	for ( ; ; ) {
		pause();
	}	
	return ((void*)0);
}

//thread that checks the msg_queue
void * thread_msg_queue(void * arg) {
	char * msg_queue_msg;
	
	mutex_thr = malloc(sizeof(pthread_mutex_t *));
	/*
	if (pthread_mutex_init(mutex_thr, NULL) == -1) {
		err(1, "error while initializing mutex");
	}
	*/

	for ( ; ; ) {
		msg_queue_msg = msg_queue_recieve();
		//pthread_mutex_lock(mutex_thr);
		write_err(msg_queue_msg);
		//pthread_mutex_unlock(mutex_thr);
	}	
	return ((void*)0);
}

void manage_resources() {
	struct rlimit * rlim = malloc(sizeof(struct rlimit *));

	//first check the current value
	if (getrlimit(RLIMIT_NOFILE, rlim) == -1) {
		err(1, "error while reading resource limit");
	}

	//printf("current soft limit : %d\n", (int) rlim->rlim_cur);
	//printf("current hard limit : %d\n", (int) rlim->rlim_max);

	/*
	--> this is for testing only

	rlim->rlim_cur = 3;
	setrlimit(RLIMIT_NOFILE, rlim);

	if (getrlimit(RLIMIT_NOFILE, rlim) == -1) {
		perror("adsf");
		err(1, "error while reading resource limit");
	}

	printf("current soft limit : %d\n", (int) rlim->rlim_cur);
	printf("current hard limit : %d\n", (int) rlim->rlim_max);
	*/
	
	//make sure that at least 10 files can be opened at the same time
	if (rlim->rlim_cur < 10) {
		
		rlim->rlim_cur = 10;

		if (setrlimit(RLIMIT_NOFILE, rlim) == -1) {
			//could not fix the problem, terminate
			err(1, "there are not enough resources on this maschine to execute the cleaner");
		}
	}
}



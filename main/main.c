#include "core.h"

//global variables that are needed for semas, msg queues, shared memory, fifo and resource management
key_t sem_key = 0;
int sem_id_error = 0;
pid_t current_child_pid = 0;

key_t ipc_key;
int msg_queue_id;
int fifo_pd;
int shm_id;
char * shm_buffer;
struct slock * sema_lock;

pthread_mutex_t * mutex_thr;

int main (int argc, char * args[]) {

	int i;
	pid_t child_pid;
	pthread_t tid1, tid2;

	int toExecute[FUNCTIONS_COUNT+1];
	char * path;
	char * fifo_buf;

	//make sure that enough resources are available
	manage_resources();
	
	//procede arguments
	path = proc_args(argc, args, toExecute);

	//prepare msg queue to recieve information from children
	msg_queue_prepare();
	msg_queue_setup_queue();

	//prepare fifo to recieve information from children
	fifo_mk();
	fifo_open_for_read();

	//prepare shared memory
	shm_create();
	shm_connect();

	//start the thread that listens for problems in child processes
	pthread_create(&tid1, NULL, thread_signals, NULL);
	//start the thread that listens to msg_queue
	pthread_create(&tid2, NULL, thread_msg_queue, NULL);


	//spawn the child processes sequentially	
	i = 0;
	while (toExecute[i] != 0 && i < FUNCTIONS_COUNT) {
		
		current_child_pid = spawn_child(toExecute[i], path);
		
		//check if child put information in fifo
		fifo_buf = fifo_read();
		if (fifo_buf != NULL) {
			//printf("This was found in the fifo : %s\n", fifo_buf);
			//write the content of the fifo to the errorlog file
			write_err(fifo_buf);
		}		

		//check the shared memory only after the execution of process 2
		if (toExecute[i] == 2) {
			//write the content of the shared memory to the stat file
			write_stat_file(shm_read());
		}

		i++;
	}

	//cleanup
	fifo_close();
	shm_disconnect();
	shm_delete();

	return(0);
}

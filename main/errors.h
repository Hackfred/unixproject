#include <stdio.h>
#include <stdlib.h>
#include <sys/sem.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <time.h>

//semas
extern key_t sem_key;
extern int sem_id_error;

//errors
extern pid_t current_child_pid;

//errors
extern void write_err(char s[100]);
extern void err(int code, char * msg);
extern void setup_sem_error();

//semas
extern void p(int sem_id);
extern void v(int sem_id);
extern int new_sem(int value);

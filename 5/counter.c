#include "./counter.h"

//globals
key_t sem_key = 0;
int sem_id_error = 0;
pid_t current_child_pid = 0;

struct command * head = NULL;

//linked list
struct command {
	int count;
	char * command;
	struct command * next;
};

void init_list(struct command ** head) {
	int i = 0;
	struct command * c;

	while (all_cmds[i] != NULL) {
		c = malloc(sizeof(struct command));

		c->count = 0;
		c->command = malloc(sizeof(char *) * strlen(all_cmds[i]));
		strcpy(c->command, all_cmds[i]);
		c->next = *head;

		(*head) = c;
		i++;
	}
}

char * concat(const char * s1, const char * s2) {
	char * r = malloc(strlen(s1) + strlen(s2) + 1);
	strcpy(r, s1);
	strcpy(r, s2);
	return r;
}

void empty_array(char * a, int n) {
	int i=0;

	while (i < n && a[i] != '\0') {
		a[i] = '\0';
		i++;
	}
}

void count_command(char * s) {
	struct command * c = head;

	while (c != NULL) {
		if (strcmp(c->command, s) == 0) {
			c->count += 1;
			return ;
		} else {
			c = (c->next);
		}
	}
}

void print_list() {
	struct command * c = head;
	int a, b = 0, d = 0;
	char * missing[200];


	printf("These command have been used\n");
	while (c != NULL) {
		if (c->count > 0) {
			printf("%25s %5d\n", c->command, c->count);
			d++;
		} else {
			missing[b] = malloc(sizeof(char) * strlen(c->command));
			strcpy(missing[b], c->command);
			b++;
		}
		c = c->next;
	}

	//printf("These commands have not been used\n");
	for (a = 0; a < b; a++) {
		//printf("%s\n", missing[a]);
	}

	printf("In total %d commands have been used\n", d);
	//printf("%d out of %d total commands have not been used yet\n", b, (b+d));
}

void perform_command_search(int fd) {
	int n, i, l = 0, b, bmax;
	char s[100];
	char line[500];
	char cmd[25];
	int a, last_space = 0;

	//always try to process whole lines 
	while ((n = read(fd, s, 100)) > 0) {
		i = 0;
		while (i < n) {
			if (s[i] == '\n') {
				for (a = 0; a < l; a++) {
					if (line[a] == '(') {
						b = 1;
						while (((line[a-b] >= 65 && line[a-b] <= 122) || (line[a-b] >= 48 && line[a-b] <= 57)) && (a-b) >= 0) {
							b++;
						}
						b--;
						bmax = b;
						while (b > 0) {
							cmd[bmax - b] = line[a-b];
							b--;
						}

						count_command(cmd);
						//printf("command found: %s\n", cmd);
						empty_array(cmd, 25);
					}
				}
				l = 0;
			} else if (l < 500) {
				line[l] = s[i];
				l++;
			} else {
				err(1, "line is longer than 500 characters. this is not permitted in counter.c");
			}
			i++;
		}
	}
	
	if (n < 0) {
		perror("read error");
	}
	
}

void rec_dir_search(char * path) {
	DIR * odir;
	struct dirent * dir;

	int l, fd;
	
	struct stat buf;

	char * fullpath;

	fullpath = malloc(sizeof(char *) * (strlen(path) + 50));

	if ((odir = opendir(path)) == NULL) {
		err(1, "Could not open specified directory in command counting process");
	}
	while ((dir = readdir(odir)) != NULL) {
		sprintf(fullpath, "%s/%s", path, dir->d_name);
		if (lstat(fullpath, &buf) == -1) {
			perror("stat error");
			err(1, "Stat error in counting process");
		}
		if (S_ISREG(buf.st_mode)) {
			//check if file is a .c file
			l = strlen(dir->d_name);
			if ((dir->d_name)[l-2] == '.' && (dir->d_name)[l-1] == 'c') {
				if ((fd = open(fullpath, O_RDONLY)) < 0) {
					err(1, "Could not open file in counting process");
				}
				perform_command_search(fd);
				//printf("%s will be searched\n", fullpath);
			}
		} else if (S_ISDIR(buf.st_mode)) {
			//printf("%s\n", dir->d_name);
			int a = strcmp(".", dir->d_name);
			if ((strcmp(".", dir->d_name) != 0) && (strcmp("..", dir->d_name) != 0)) {
				char * c_path = concat(path, "/");
				rec_dir_search(concat(c_path, fullpath));	
			}
		} 		
	}
}

int main (int argc, char * argv[]) {


	if (argc == 1) {
		err(1, "For command counting specify the path of the starting directory as an argument");
	} else {
		init_list(&head);
		rec_dir_search(argv[1]);	
		print_list();
	}

	return 0;
}

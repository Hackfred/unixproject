#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>

#include "../main/errors.c"

//commands that are mentioned in the book, but that are depreached and unsecure got removed from this list
//getlogin
//vfork

const char * all_cmds[160] = {"open","creat","close","lseek","read","write","pread","pwrite","dup","dup2","fsync","fdatasync","sync","fcntl","ioctl","stat","fstat","lstat","access","umask","chmod","fchmod","chown","fchown","lchown","truncate", "ftruncate", "link","unlink","remove","rename","symlink","readlink","utime","mkdir", "rmdir","opendir", "readdir", "rewinddir", "closedir", "chdir", "fchdir", "getcwd","main", "return","exit","_Exit","_exit","pthread_exit","abort","atexit","malloc","calloc","realloc","free","alloca","getenv","putenv","setenv","unsetenv","setjmp","longjmp","getrlimit","setrlimit","getpid","getppid","getuid","geteuid","getgid","getegid","fork","wait","waitpid","waitid","wait3","wait4","execl","execv","execle","execve","execlp","execvp","setuid","setgid","setreuid","setregid","seteuid","setegid","system","times","getpgrp","getpgid","setpgid","setsid","getsid","tcgetpgrp","tcsetpgrp","kill","raise","signal","alarm", "pause","sigemptyset","sigfillset","sigaddset","sigdelset","sigismember", "sigprocmask","sigpending","sigaction","sigsuspend", "sleep","pthread_equal","pthread_self","pthread_create","pthread_join","pthread_cancel","pthread_cleanup_push","pthread_cleanup_pop","pthread_mutex_init","pthread_mutex_destroy","pthread_mutex_lock","pthread_mutex_trylock", "pthread_mutex_unlock","pipe","popen","pclose","mkfifo","ftok","msgget","msgctl","msgsnd","msgrcv","semget","semctl","semop","shmget","shmctl","shmat","shmdt","sem_open","sem_close","sem_unlink","sem_trywait","sem_wait","sem_timedwait","sem_post","sem_init","sem_destroy", "sem_getvalue", NULL};



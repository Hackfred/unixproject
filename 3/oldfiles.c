#include <stdio.h>
#include <err.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <libgen.h>
#include <assert.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <limits.h>

/* colors */
#define COLOR_RED     "\x1b[31m"
#define COLOR_GREEN   "\x1b[32m"
#define COLOR_YELLOW  "\x1b[33m"
#define COLOR_BLUE    "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN    "\x1b[36m"
#define COLOR_RESET   "\x1b[0m"



#define MAKE_PATH(path) strcpy(path, dir_name);       \
              if(*(dir_name + strlen(dir_name) - 1) != '/') \
              strcat(path, "/");        \
                strcat(path, p->d_name)

#define GASP(n) register int i;       \
    if(g) putchar('-');      \
                for(i = 0; i < n; i++)      \
                  if( i != 0 && !(i % 4))   \
                    putchar('-');      \
                  else          \
                    putchar(' ')
                    

#define POINT fprintf(stdout, "%s", "-> ")

#define MAX_OLD_FILES 200

int getTimeDifference(time_t lastAccess);
void showList(char *dir_name, int g);
void saveFilePath(char* path, int i);
void splitString(char * str);
void removeFiles();
int separate_numbers[MAX_OLD_FILES];
int amount_of_files = 0;
int amount_of_months;
char *files[100000];
int counter = 0;
int size = 0;


int main(int argc, char **argv) {
  
  printf ("How old should the file be in months if you would like to remove it? ");
  scanf ("%d",&amount_of_months);
	fgetc(stdin);
  if(argc < 2)
    showList("./", 0);
  else
    showList(argv[1], 0);

  printf("\n");
  for(int i = 1; i < (counter + 1); ++i) {
    printf("%d: %s\n", i, files[(i - 1)]);
  }

  if(amount_of_files > 0) {
    printf ("\nPlease select which files you want to remove by number and separate them by an comma(,) ");
    char * numbers = malloc(sizeof(char) * MAX_OLD_FILES);
    fgets(numbers, MAX_OLD_FILES, stdin);
    printf("the numbers you entered is: %s\n", numbers);
    splitString(numbers);
    removeFiles();
  } else {
    printf("There are no old files");
  }

  return 0;
}


void splitString(char * str) {

  char * pch;
  int counter = 0;
  pch = strtok(str," ,.-");

  while (pch != NULL)
  {
    separate_numbers[counter] = atoi(pch);
    pch = strtok (NULL, " ,.-");
    counter++;
    size++;
  }
}

void removeFiles() {
  
  char fResponse = 'n';  
  char *name = malloc(sizeof(char) * 1024);
  int loop;
  int status;
  
  for(loop = 0; loop < size; loop++) {
   	 
     name = basename(files[(separate_numbers[loop] - 1)]);
     printf("\nAre you sure to delete file: %s? (y/n) ", name);
     scanf("%c", &fResponse);
		 fgetc(stdin);
     if(fResponse == 'y') {

        if(unlink(files[(separate_numbers[loop] - 1)]) < 0 )   
        {
            perror("remove failed");  
        } else {
            printf("\nRemoved file: '%s'\n", name);
        }
      } else {
        printf("\nFile: '%s' not removed\n", name);
      }
  }
      
}


void saveFilePath(char *path, int i){
  files[i] = malloc(sizeof(char *) * strlen(path));
  strcpy(files[i], path);
  amount_of_files++;
}

int getTimeDifference(time_t lastAccess){
    int months;
    long current_month;
    long current_year;
    long last_access_month;
    long last_access_year;
    struct tm *current_tm;
    struct tm *last_access_tm;
    
    time_t current = time(NULL);
    
    current_tm = localtime(&current);
    current_year = (current_tm->tm_year + 1900);
    current_month = (current_tm->tm_mon + 1);

    last_access_tm = localtime(&lastAccess);
    last_access_year = (last_access_tm->tm_year + 1900);
    last_access_month = (last_access_tm->tm_mon + 1);

    if((current_year - last_access_year) == 0){
        months = current_month - last_access_month;
    } else if((current_year - last_access_year) == 1) {
        months = ((12 - last_access_month) + current_month);
    } else {
        months = (((current_year - last_access_year) - 1) * 12);
        months = (months + ((12 - last_access_month) + current_month));
    }
    return months;
}


void showList(char *dir_name, int g) {
  DIR *cur_dir;   
  struct dirent *p; 

  if((cur_dir = opendir(dir_name)) == NULL)
     perror("Can't open directory");

  while((p = readdir(cur_dir)) != NULL) {
    struct stat st_buf;
    char *path;

    if(*p->d_name == '.') continue;

    if((path = malloc(_POSIX_PATH_MAX)) == NULL)
       perror("Malloc error");

    MAKE_PATH(path);

    if(lstat(path, &st_buf) != EOF) {
      static char head;

        if(!head++)
          fprintf(stdout, "%s[%s]%s\n", COLOR_GREEN, dir_name, COLOR_RESET);

        time_t lastAccess = st_buf.st_mtime;
        int months = getTimeDifference(lastAccess); 
          
        if(S_ISREG(st_buf.st_mode) && (months < amount_of_months)) {
          GASP(g);
          POINT;
          fprintf(stdout, "%s %s(%lld k)%s\n", p->d_name, COLOR_BLUE, st_buf.st_size, COLOR_RESET);
        } else if(S_ISREG(st_buf.st_mode) && (months >= amount_of_months)){
          GASP(g);
          POINT;
          fprintf(stdout, "%s%s %s(%lld k)%s\n", COLOR_RED, p->d_name, COLOR_BLUE, st_buf.st_size, COLOR_RESET);
          if(amount_of_files > MAX_OLD_FILES){
              printf("\nProgram stopped because there are too many old files in this directory");
              exit(0);
          }
          saveFilePath(path, counter);
          counter++;
        }

        else if(S_ISDIR(st_buf.st_mode)) {
          GASP(g);
          POINT;
          fprintf(stdout, "%s[%s]%s\n", COLOR_GREEN, p->d_name, COLOR_RESET);
          showList(path, g+4);
        }

        else {
          perror("Unknown file type");
				}
    }
    
    else 
       perror("Stat error");

    free(path);
  }

  closedir(cur_dir);
}

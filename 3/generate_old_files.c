#include <stdio.h>
#include <dirent.h>
#include <errno.h>   // for errno
#include <limits.h>  // for INT_MAX
#include <stdlib.h>  // for strtol
#include <string.h>
#include <sys/types.h>
#include <utime.h>
#include <time.h>

int main(int argc, char *argv[]) 
{
	char *filename = "oldfile";
	int SIZE_CATED_STRING = 8;
	char *path = argv[1];
	struct utimbuf ubuf;
	struct tm time;
	errno = 0;
	char *p = malloc(sizeof(char)*10);
	int num;
	int i;
	
	long conv = strtol(argv[2], &p, 10);

	if (errno != 0 || *p != '\0' || conv > INT_MAX) {
    	printf("Please put in the right parameters");
    	exit(0);
	} else {
    
    num = conv; 
    strcat(path, filename);   
    
    if (strptime("6 Dec 2016 12:33:45", "%d %b %Y %H:%M:%S", &time) == NULL) {
    	perror("Something went wrong with the date"); 
    	exit(2);
    }
	
	time_t t = mktime(&time);

	printf("Generating old files...\n\n");

    for(i = 0; i < num; i++){
    	SIZE_CATED_STRING = (strlen(path) + 1);
    	char cated_string[SIZE_CATED_STRING];
		sprintf(cated_string,"%s%d",path,i);
    	printf("%s\n", cated_string);
    	FILE* file_ptr = fopen(cated_string, "w");
    	fclose(file_ptr);	
    	ubuf.modtime = t;
    	utime(cated_string, &ubuf);
    }
	}
	return 0;
}
#include "fcntl.h"
#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"

char buf1[] = "a";
char buf2[] = "ABCDEFGHIJ";
char buf3[] = "W E R K T ";

int main(int argc, char *argv[]) {
int i;
for(i = 1; i < argc; i++){
  int fd;
  if ((fd = creat(argv[i], 0700)) < 0) printf("creat error");
  if (write(fd, buf1, 1) != 1) printf("buf1 write error"); /* offset now = 1 */

  if (lseek(fd, 50, SEEK_SET) == -1) printf("lseek error"); /* offset now = 50 */
  if (write(fd, buf3, 10) != 10) printf("buf2 write error"); /* offset now = 60 */

  if (lseek(fd, 90, SEEK_SET) == -1) printf("lseek error"); /* offset now = 90 */
  if (write(fd, buf2, 10) != 10) printf("buf2 write error"); /* offset now = 100 */

}
  exit(0);

}

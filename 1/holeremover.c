#include "stdio.h"
#include "stdlib.h"
#include "fcntl.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "dirent.h"
#include "string.h"
#include "unistd.h"

char *files[200];
int counterfiles;
int totalbytessaved = 0;

void deleteHoles(char *file){
	int bytessaved = 0;

	int count;
	
	int fd, fd2;
	int i, n, a;
	int flag = 0;
	char *tempfile = "tempfile.txt";
	char *oldname = file;
	int holecounter = 0;
	

	printf("\nCurrently working on file: %s \n", oldname);
	
	if((fd = open(file, O_RDONLY)) < 0){
		perror("error opening file");
	}
	
	int filesize;
	struct stat st;
	stat(file, &st);
	filesize = st.st_size;
	printf("file size before removing holes: %d\n", filesize);

	char buf[filesize], buf2[filesize];
	
	if((fd2 = open(tempfile, O_WRONLY | O_CREAT |O_TRUNC, S_IRUSR | S_IWUSR)) < 0){
		perror("Error opening new file");
	}

	while((n = read(fd, buf, filesize)) > 0){
		i = 0;
		a = 0;
		while(i < filesize){
			if(buf[i] != '\0'){
				buf2[a] = buf[i];
				flag = 0;
				a++;
			}else{
				if(flag == 0){
					/*printf("found hole\n");*/
					flag = 1;
					holecounter++;
				}
			}
			i++;
		}
		if (a > 0){
			if(write(fd2, buf2, a) != a){
				perror("write error");
			}
		}
	}
	if(holecounter > 0){
		if((remove(file)) != 0){
			perror("file couldn't be removed");
		}
		if((rename(tempfile, oldname)) ==-1){
			perror("file couldn't be renamed");
		}
	printf("Found %d holes in file %s, all holes have been succesfully removed.\n", holecounter, oldname);


	
		int newfilesize;
		struct stat st2;
		stat(oldname, &st2);
		newfilesize = st2.st_size;
		bytessaved = filesize - newfilesize;

		printf("New file size: %d \n", newfilesize);
	}else{
		if((remove(tempfile)) != 0){
			perror("file couldn't be removed");
		}
		printf("no holes found on this file\n");
	}
	

	if(bytessaved > 0){
	totalbytessaved += bytessaved;
		printf("Sucessfully finished and saved %d in bytes\n", bytessaved);
	}
}


void findfiles(char *dir, int depth){
    DIR *dp;
    struct dirent *entry;
    struct stat statbuf;
    if((dp = opendir(dir)) == NULL) {
        fprintf(stderr,"cannot open directory: %s\n", dir);
        return;
    }
    chdir(dir);
    while((entry = readdir(dp)) != NULL) {
        lstat(entry->d_name,&statbuf);
        if(S_ISREG(statbuf.st_mode)) {
        char *suffix = strrchr(entry->d_name,'.');
        if(suffix){
        	suffix++;
        	if(strcasecmp(suffix, "txt") == 0){
         		deleteHoles(entry->d_name);
        	}
        }
      } else if (S_ISDIR(statbuf.st_mode)){
      	/* Found a directory, but ignore . and .. */

            if(strcmp(".",entry->d_name) == 0 ||
                strcmp("..",entry->d_name) == 0)
               continue;

            //printf("%*s%s/\n",depth,"",entry->d_name);
	    
            /* Recurse at a new indent level */
            findfiles(entry->d_name,depth+4);
      }
    }
    chdir("..");
    closedir(dp);
}


int main(int argc, char *argv[]){
	
	findfiles(argv[1],0);
	
	if(totalbytessaved > 0){
		printf("\nAll holes have been removed and %d bytes have been deleted\n",totalbytessaved);
	}
	return 0;
}

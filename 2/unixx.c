#include <sys/types.h>
#include <errno.h>
#include <limits.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <math.h>
#include <string.h>
#include <setjmp.h>
#include <time.h>
#include <stdbool.h>
#include <sys/time.h>
#include <pthread.h>
#include <signal.h>
#include "../main/ipc.h"
// Display information function
// This function will be called with a start directory path
// argv[1] == path

void signal_handler(int);
void handle_signal(int);
//For Semaphore
struct slock *sema_lock;
key_t sem_key = 0;
int sem_id_error = 0;
pid_t current_child_pid = 0;

//for message queue
key_t ipc_key;
int msg_queue_id;

//For fifo
int fifo_pd;

// For Shared memory
int shm_id;
char *shm_buffer;

int num;
char *name;
char *logfilename = "unix_programming_logfile.txt";
pthread_t t1;
bool my_log;
char *filename;
char *search_array;
char **permission;
int file_uid;
int global_filecount;
int global_dircount;

void write_logfile(char*tmp);

// function for creating current timestamp for logfile
char *time_stamp(){

	char *timestamp = (char *)malloc(sizeof(char) * 16);
	time_t ltime;
	ltime=time(NULL);
	struct tm *tm;
	tm=localtime(&ltime);
	
	sprintf(timestamp,"%04d%02d%02d%02d%02d%02d", tm->tm_year+1900, tm->tm_mon + 1, 
    	tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
	return timestamp;
}

char *timetostring(time_t t){

	char *ret_time = (char *)malloc(sizeof(char) * 32);
	struct tm *tm;
	tm = localtime(&t);
	sprintf(ret_time,"%04d-%02d-%02d %02d:%02d:%02d", tm->tm_year+1900, tm->tm_mon + 1,
        tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
	return ret_time;
}

// Function that search recursive through a path and prints either files, directories or both, depend on number value 
void listdir(const char *name, int i, int number){

	DIR *dir;
	struct dirent *entry;
	dir = opendir(name);
	rewinddir(dir);
	char tmp[1024]; 
	while((entry = readdir(dir)) != NULL){
		if(entry->d_type == DT_DIR){
			// Directory
			char path[1024];
			if(!(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)){
				snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
				if(number == 1 || number == 3){
					printf("\033[32m%*s[%s]\n", i, "", entry->d_name);
					sprintf(tmp, "%*s[%s]\n", i, "", entry->d_name);
					global_dircount++;
					if(my_log){
						write_logfile(tmp);
					}
				}
				listdir(path, i + 2, number);
			}
		} else{
			// Files
			if(number == 3){
				printf("\033[m%*s- %s\n", i, "", entry->d_name);
				sprintf(tmp, "%*s- %s\n", i, "", entry->d_name);
				global_filecount++;
				if(my_log){
                                         write_logfile(tmp);
                                }
	

			} else if(number == 2){
				printf("\033[m- %s\n",entry->d_name);
				sprintf(tmp, "- %s\n",entry->d_name);
				global_filecount++;
	                	if(my_log){            
			   		write_logfile(tmp);
				}
			}
			
		}	
	}
	closedir(dir);
}

// Function that search recursivly through the path for a certain file
void dir_search(char *name, char *array){

	
	DIR *dir;
        struct dirent *entry;
        dir = opendir(name);
        while((entry = readdir(dir)) != NULL){
                if(entry->d_type == DT_DIR){
                        // Directory
                        char path[1024];
                        if(!(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)){
                                snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
                                dir_search(path, array);
                        }
                } else{
                        // Files
            		if(strcmp(entry->d_name, filename) == 0){
				chdir(name);
				realpath(filename, array);	
                        }

          }

	}
	closedir(dir);

}

int binTOoct(long long b){

	int octal = 0;
	int decimal = 0;
	int i = 0;
	while(b != 0){
		
		decimal += (b%10) * pow(2,i);
		i++;
		b /=10;
	}
	i = 1;
	while(decimal != 0){
		
		octal += (decimal % 8)*i;
		decimal /= 8;
		i *= 10;
	}
	return octal;		

}



void print_perm(_mode){

char *output[12] = {"User: " ,"-", "-", "-"," Group: ", "-", "-", "-"," Others: ", "-", "-", "-"};

		if (_mode & S_IRUSR)
			output[1] = "r";

		if (_mode & S_IWUSR) 
			output[2] = "w";

		if (_mode & S_IXUSR)
			output[3] = "x";

		if (_mode & S_IRGRP)
			output[5] = "r";

		if (_mode & S_IWGRP) 
			output[6] = "w";

		if (_mode & S_IXGRP)
			output[7] = "x";

		if (_mode & S_IROTH)
			output[9] = "r";

		if (_mode & S_IWOTH) 
			output[10] = "w";

		if (_mode & S_IXOTH)
			output[11] = "x";
		int i;
		printf("Permissions => ");
		for(i = 0; i < 12; i++){	
			printf("%s", output[i]);
		}
		printf("\n");
		for(i = 0; i < 12; i++){
			permission[i] = output[i];
		}


}

void print_details(int which, char *array){

	int fd;
	int i_node;
	int uid;
	int gid;
	int size;
	time_t t;
	struct stat buf;
	char tmp_buf[100];
	char *callptr = malloc(sizeof(char) * 100);
	char *for_call = realloc(callptr, sizeof(char) * 300);
	int anzahl_number =  sprintf(tmp_buf, "%d", which);
		
	if(lstat(array, &buf) < 0){
		perror("lstat error\n");
		exit(1);
	}
	int i = 0;
	file_uid = buf.st_uid;
	for(i = 0; i < anzahl_number; i++){
		switch(tmp_buf[i]){
			case '1':
				//Permissions
				print_perm(buf.st_mode);	
				int i = 0;
				write_logfile("\nPermissions => ");
				for(i = 0; i < 12; i++){
					write_logfile(permission[i]);	
				}
				break;
			case '2':
				// I -Node Number
				i_node  = buf.st_ino;
				printf("I-Node number: %d\n", i_node);
				sprintf(for_call, "\nI-Node number: %d", i_node);
				write_logfile(for_call);
				break;
			case '3':
				// USER ID of owner
				uid = buf.st_uid;
				printf("User ID of owner: %d\n", uid);
				sprintf(for_call, "\nUser ID of owner: %d", uid);
				write_logfile(for_call);
				break;
			case '4':
				//Group ID of owner
				gid = buf.st_gid;
				printf("Group ID of owner: %d\n", gid);
				sprintf(for_call, "\nGroup ID of owner: %d", gid);
				write_logfile(for_call);     
                                break;
                        case '5':
				// Size in bytes
				size = (int) buf.st_size;
				printf("Size in bytes: %d\n", size);
				sprintf(for_call, "\nSize in bytes: %d", size);
				write_logfile(for_call);
                                break;
			case '6':
				// Time of last access
				t =  buf.st_atime;
				for_call = timetostring(t);
				printf("Time of last access: %s\n", for_call);
				write_logfile("\nTime of last access: ");
				write_logfile(for_call);
                                break;
                        case '7':
				// Time of last modification
				t = buf.st_mtime;
				for_call = timetostring(t);
				printf("Time of last modification: %s\n", for_call);
				write_logfile("\nTime of last modification: ");
				write_logfile(for_call);
                                break;

		}
	}

}


// Write to logfile
void write_logfile(char *tmp){

int fd;
int count;
int i = 0;

// logfilename is timestamp + logfilename
if(num == 2){
	fd = open(name, O_RDWR | O_APPEND);
	count = write(fd, tmp, strlen(tmp));

// logfilename is just logfilename
} else {
	fd = open(logfilename, O_RDWR | O_APPEND);
	count = pwrite(fd, tmp, strlen(tmp),0);
}
close(fd);

}

void cleanup(void *arg){

	free(arg);	

}

// Thread function
void *thread_function(){

printf("\033[mDo you want to know more details about a certain file? (y/n)\n");
int t_id = pthread_self();
char *tmp_send = malloc(sizeof(char) * 20);
sprintf(tmp_send, "%d", t_id);

msg_queue_send(tmp_send);

// Cleanup handler register 
pthread_cleanup_push(cleanup, tmp_send);
// cleanup handler call
pthread_cleanup_pop(1);

pthread_exit(0);

}

//Signal Handler for SIGUSR1
void signal_handler(int sig){

	printf("\nWhat information do you want to know?\n");
	

}

// SIGNAL Handler for SIGUSR2
void handle_signal(int signal){

	system("date");

}



// main function
int main(int argc, char *argv[]){

	struct sigaction sa;
	sigset_t mask;
	memset(&sa, '\0', sizeof(sa));
	sa.sa_handler= &handle_signal;
	sigemptyset(&mask);
	sigaddset(&mask, SIGINT);
	sigaction(SIGUSR2, &sa, NULL);
	struct timeval begin, end;
	long seconds, mikro_seconds;
	global_filecount = 0;
	global_dircount = 0;
	
	msg_queue_prepare();
	msg_queue_get_queue();
	msg_queue_send("Status: Start of Task2\n");
	if(sigismember(&mask, SIGINT) == 1){	
		msg_queue_send("SIGINT add\n");
	}
	char *fifo_buf;
	fifo_open_for_write();

	shm_connect();
	
	fifo_write("FIFO FOR ERROR REPORTING\n");
	
	char *string_tmp_send = malloc(sizeof(char) * 50);
        sprintf(string_tmp_send, "Process Group ID: %d or %d\n", getpgrp(), getpgid(0));
	msg_queue_send(string_tmp_send);
	free(string_tmp_send);

	string_tmp_send = malloc(sizeof(char) * 50);
	sprintf(string_tmp_send, "Process Group ID of session leader: %d\n", getsid(0));
	msg_queue_send(string_tmp_send);

	raise(SIGUSR2);
	sigdelset(&mask, SIGINT);
	free(string_tmp_send);

	signal(SIGUSR1, signal_handler);
	
	char cwd[256];
	getcwd(cwd, sizeof(cwd));
	struct stat buffer;
	if(lstat(argv[1], &buffer) < 0){
		perror("lstat error: No path\n");
		fifo_write("LSTAT ERROR: NO PATH\n");
		_Exit(1);
	}
	if(!(S_ISDIR(buffer.st_mode))){
		printf("Your argument is not a directory path!\n");
		fifo_write("ARGUMENT IS NOT A DIRECTORY PATH\n");
		_exit(1);
	}
	
	// Create tmp_folder for some stuff
	char *tmp_dir = malloc(sizeof(char) * 256);
	sprintf(tmp_dir, "%s/tmp_folder", cwd);	
	mkdir(tmp_dir, 0777);
		
	permission = malloc(sizeof(char*) * 12);
	int lauf;
	for(lauf = 0; lauf < 12; lauf++){
		permission[lauf] = malloc(sizeof(char) * 20);
	}
        char *val = malloc(sizeof(char) * 100);
	name = calloc(1024, sizeof(char));
	search_array = malloc(sizeof(char) * 1024);
	bool no_logfile = true;
        jmp_buf current_program;
        printf("\033[1mIn this function we display different information\n\n");
	printf("\033[mDo you want to create a logfile for later? (y/n)\n");
	setjmp(current_program);
        scanf("%s", val);
	fgetc(stdin);
	if(strcmp("n", val) != 0 && strcmp("y", val) != 0){
                printf("Wrong input! y or n\n");
		longjmp(current_program, 1);
	}
	long location_dir;
	DIR *directory = opendir(".");
        struct dirent *dir;
	if(strcmp(val, "y") == 0){        
			// Log file should be created
			// Check if old log file is in the current directory 
			my_log = true;
			string_tmp_send = malloc(sizeof(char) * 50);
			location_dir = telldir(directory);
			sprintf(string_tmp_send, "Current DIR location: %ld", location_dir);
			msg_queue_send(string_tmp_send);
			free(string_tmp_send);
			msg_queue_send("Status: User wants a logfile\n");
			while((dir = readdir(directory)) != NULL){
				
				// Log file exists
				if(strcmp(dir->d_name, logfilename) == 0){
					no_logfile = false;
					printf("\nOld logfile exists, what do you want to do?\n\n");
					printf("Delete old logfile and create a new one (Input = 1)\n");
					printf("Create a new logfile with current timestamp (Input = 2)\n");
					printf("Overwrite the old logfile (Input = 3)\n");
					scanf("%d", &num);
					fgetc(stdin);
					while((num != 1) && (num != 2) && (num != 3)){
						printf("Enter 1, 2 or 3\n");
						scanf("%d", &num);
						fgetc(stdin);
					}

					switch(num){
					char *time = (char *)malloc(sizeof(char) * 16);
					int fd;
						case 1:
							remove(logfilename);
							umask(0);
							fd = open(logfilename, O_RDWR | O_CREAT, 0777);
							if(access(logfilename, (R_OK | W_OK | X_OK) != 0)){
								fifo_write("CREATING LOGFILE ERROR\n");
								perror("Creating logfile error!\n");
							} 
							close(fd);
							break;
						case 2:	
							time =  time_stamp();
							sprintf(name, "%s_%s", time, logfilename);
							fd = open(name, O_RDWR | O_CREAT, 0777);
							if(access(logfilename, (R_OK | W_OK | X_OK) != 0)){
								fifo_write("CREATING LOGFILE ERROR\n");
								perror("Creating logfile error!\n");
							}
							close(fd);
							free(time); 
							break; 
						case 3:
							fd = open(logfilename, O_RDWR);
							if(ftruncate(fd, 0) < 0){
								perror("truncate error");
								fifo_write("TRUNCATE ERROR\n");
							}
							close(fd);
							break;
						
					}
			
				}
				
			}
			if(no_logfile){
				umask(0);
                        	int fd = open(logfilename, O_RDWR | O_CREAT, 0777);
                      		if(access(logfilename, (R_OK | W_OK | X_OK) != 0)){
                                 	fifo_write("CREATING LOGFILE ERROR\n");
					perror("Creating logfile error!\n");
                       		 }
                        close(fd);
			}
			write_logfile("Path is: ");
	       		write_logfile(argv[1]);
       			write_logfile("\n");



			closedir(directory);
			

		} else if(strcmp(val, "n") == 0){
			my_log = false;	
			msg_queue_send("Status: User wants no logfile\n");
		}
		

	// Done with the logfile part
	// Now the actual part comes
	int number;
	msg_queue_send("Status: Listing\n");
	printf("\nWhat do you want to do now?\n"); 
	printf("List all directories within your path? (Input = 1)\n");
	printf("List all files within your path? (Input = 2)\n");
	printf("List all directories and files within your path? (Input = 3)\n");
	scanf("%d", &number);
	fgetc(stdin); 
	while((number != 1) && (number != 2) && (number != 3)){                  
                      printf("Enter 1, 2 or 3\n");
	              scanf("%d", &number);
                      fgetc(stdin);
        }
	gettimeofday(&begin, NULL);
	listdir(argv[1], 0, number);
	gettimeofday(&end, NULL);
	
	seconds = end.tv_sec - begin.tv_sec;
	mikro_seconds = end.tv_usec - begin.tv_usec;
	char *sm_second = malloc(sizeof(char) * 212);
	char *sm_mikro = malloc(sizeof(char) * 212);
	sprintf(sm_second, "%ld", seconds);
	sprintf(sm_mikro, "%ld", mikro_seconds);
	
	char *sm_dir = malloc(sizeof(char) * 212);
	char *sm_file = malloc(sizeof(char) * 212);
	sprintf(sm_dir, "%d", global_dircount);
	sprintf(sm_file, "%d", global_filecount);

	char *final = malloc(sizeof(char) * 200);	
	sprintf(final, "Verstrichene Zeit:\nSekunden: %s\nMikrosekunden: %s\nAnzahl DIR: %s\nAnzahl FILES: %s\n", sm_second, sm_mikro, sm_dir, sm_file);
	shm_write(final);

	free(sm_second);
	free(sm_mikro);
	free(sm_dir);
	free(sm_file);
	free(final);
	
	int err;
	err = pthread_create(&t1, NULL, thread_function, NULL);
	if(err != 0){
		fifo_write("CANT CREATE THREAD\n");
		perror("cant create!\n");
	}
		 
	pthread_join(t1, NULL);

	scanf("%s", val);
        fgetc(stdin);
        while((strcmp("n", val) != 0 && strcmp("y",val) != 0)){
                printf("\033[mWrong input! y or n\n");
                scanf("%s", val);
		fgetc(stdin);
        }
	filename = malloc(sizeof(char) * 100);
	char *array = malloc(sizeof(char) * 512);
	struct stat puf_er;
	if(strcmp("y",val) == 0){
	
		int c_val;
		search_array = "";
				
		printf("Please enter the exact filename\n");
		scanf("%s", filename);
		fgetc(stdin);
		dir_search(argv[1], array);
		chdir(cwd);
		if(lstat(array, &puf_er) < 0){
               		 printf("File not found!\n");
               		 exit(1);
       		}
		
		//File found
		c_val = 1;
		msg_queue_send("Status: User searched for a file\n");
		write_logfile("More information about following file: ");
		write_logfile(filename);
		
		char *read_vari = alloca(sizeof(char)* 10);
		long long in_perm;
		if(c_val == 1){ 
		
			raise(SIGUSR1);
			printf("(1) File permissions\n");
			printf("(2) I-Node number\n");
			printf("(3) User ID of owner\n");
			printf("(4) Group ID of owner\n");
			printf("(5) Size in bytes\n");
			printf("(6) Time of last access\n");
			printf("(7) Time of last modification\n");
			printf("Input for example like: 125 for 1,2 and 5\n");
			msg_queue_send("Status: User wants to know more information about the file\n");
			int input;
			scanf("%d", &input);
			fgetc(stdin);
			
			
			print_details(input, array);
			int oct;
			char oct_char[15];
			char oct_2char[15];
		 	if(getuid() == file_uid){
				printf("\nYou are the owner of the file! Do you want to change some permissions? (y/n)\n");
				scanf("%s",read_vari);
				fgetc(stdin);
				if(strcmp(read_vari, "y")== 0){
					
					write_logfile("\nUser wants to change some permissions\n");
					msg_queue_send("Status: User wants to change some permissions\n");
					printf("Current permissions: ");
					int i = 0;
					for(i = 0; i < 12; i++){
        	                                printf("%s", permission[i]);
	                                }
					printf("\n");
					printf("How do you want to set them? (Input == 9 binary digits, for example: 110110100 = rw-rw-r--)\n");
					scanf("%lld", &in_perm);
					fgetc(stdin);
					char *u = malloc(sizeof(char) * 50);
					sprintf(u, "%lld",in_perm );
					if((int)strlen(u) != 9){
						printf("Input != 9 binary digits!\n");
					} else{  
						oct = binTOoct(in_perm);
						sprintf(oct_char, "%d", oct);
						oct_2char[0] = '0';
						oct_2char[1] = oct_char[0];
						oct_2char[2] = oct_char[1];
						oct_2char[3] = oct_char[2];
						i = strtol(oct_2char, 0, 8);
					
						if(chmod(array, i) != 0){
							printf("Could not change permissions! ERROR\n");
						} else{
							printf("File permissions changed successfully!\n");
							printf("New ");
							write_logfile("Changed permissions:");
							print_details(1, array);
					}
					}
					
					
				
				} 
			}
		}
	
	}
	if(my_log){
		int file_d;
		off_t currpos;
		// logfilename is timestamp + logfilename
		if(num == 2){
		 	file_d = open(name, O_RDWR | O_APPEND);
      
		// logfilename is just logfilename
		} else {
        		file_d = open(logfilename, O_RDWR | O_APPEND);
		}
		char *msg_x = malloc(sizeof(char) * 20);
		dup2(file_d, 1);

		printf("\n--- END OF TASK 2 ---\n");
		currpos = lseek(file_d, 0, SEEK_END);
		sprintf(msg_x, "End of logfile: %d\n", (int) currpos);
		msg_queue_send(msg_x);

		free(msg_x);		

		close(file_d);

	}

	int wait_status;
	pid_t fork_pid;
	switch(fork_pid = fork()){
		case -1:
			perror("FORK ERROR");
			_Exit(1);
			break;
		case 0:
			msg_queue_send("Status: END of Task2\n");
 			fifo_close();
			shm_disconnect();
			break;
		case 1:
			wait(&wait_status);
			break;
	}
	rmdir(tmp_dir);

  return 0;
    
}  

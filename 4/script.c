#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int main(int argc, char* argv[]){
  chdir(argv[1]);
  if (creat("./aH.txt", 0664) < 0){
    perror("create error");
  }

  link("./aH.txt", "./aH1.txt");
  link("./aH.txt", "./aH2.txt");
  link("./aH.txt", "./aH3.txt");

  if (creat("./a.txt", 0664) < 0){
    perror("create error");
  }
  symlink("a.txt", "./aS1.txt");
  symlink("a.txt", "./aS2.txt");
  symlink("a.txt", "./aS3.txt");

  symlink("~/Documents", "./bS1");
  symlink("~/Documents", "./bS2");
  symlink("~/Documents", "./bS3");

  symlink("../main", "./cS1");
  symlink("../main", "./cS2");
  symlink("../main", "./cS3");

  symlink("/home/martin/Download", "./dS1");
  symlink("/home/martin/Download", "./dS2");
  symlink("/home/martin/Download", "./dS3");

  return 0;
}

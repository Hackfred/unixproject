#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <malloc.h>
#include <string.h>
#include <setjmp.h>
#include <stdbool.h>

void delHardlink(const char *dname, char* read_vari){
  struct info{
    int ino;
    char *name;
  };
  DIR *dirp = opendir(dname);
  struct info entry[256];//for comparing inode
  int index, i, j, cmp;
  bool ck[256];
  struct dirent *dir_entry;
  chdir(dname);
  if (dirp != NULL){
    for (index = 0; dir_entry = readdir(dirp); index++) {
      entry[index].ino = dir_entry->d_ino;
      entry[index].name = dir_entry->d_name;
    }
  }

  for (i = 0; i < index; i++){
    cmp = entry[i].ino;
    for (j = i + 1; !ck[j] && j < index; j++){
      if (cmp == entry[j].ino){
        ck[j] = 1;
        if(!strcmp(read_vari, "y")) printf("%s is deleted\n", entry[j].name);
        unlink(entry[j].name); //delete hardlink comparing inodes.
      }
    }
  }
}

void delSymlink(const char* dname, char* read_vari){
  int index = 0, cu = 0, up = 0, abs = 0;//up : for parent dir , abs : for absolute dir
  char *entryCu[256];//current
  char *entry[256];
  char *entryAbs[256];//absolute
  char *entryUp[256];//relative

  DIR *dirp = opendir(dname);
  chdir(dname);
  struct stat buf;
  struct dirent *dir_entry;
  while ((dir_entry = readdir(dirp)) != NULL){
    lstat(dir_entry->d_name, &buf); //stat for entry in selected dir
    if (S_ISLNK (buf.st_mode)){ //whether symlink or not
      entry[index] = (char *)malloc(sizeof(char)*256);
      readlink(dir_entry->d_name, entry[index], 256); //read name symlink pointing path
      ////////////////////////////////////////
      //find / or ../ 
      //there are two cases.
      //First, if symlink points file in same dir, we can remove this symlink.(we need function querying)
      //Second, if there are symlinks that points same file or dir. we would remove this.
      //To resolve first case, we distinguish first case symlink, using concept that if readlink() doesn't have '/' or '../' 
      //To resolve second case, we compare entries.
      if (entry[index][0] == '/'){
        entryAbs[abs] = (char *)malloc(sizeof(char)*strlen(entry[index]));
        for (int i = 0; i < abs; i++){
          if(!strcmp(entry[index], entryAbs[i])){
            if(!strcmp(read_vari, "y")) printf("%s -> %s is deleted\n", dir_entry->d_name, entry[index]);
            unlink(dir_entry->d_name);
            entryAbs[i][0] = '\0';
          }
        }
        strcpy(entryAbs[abs], entry[index]);
        abs++;
      }
      else if (entry[index][0] == '.' && entry[index][1] == '.' && entry[index][2] == '/'){
        entryUp[up] = (char *)malloc(sizeof(char)*strlen(entry[index]));
        for (int i = 0; i < up; i++){
          if(!strcmp(entry[index], entryUp[i])){
            if(!strcmp(read_vari, "y")) printf("%s -> %s is deleted\n", dir_entry->d_name, entry[index]);
            unlink(dir_entry->d_name);
            entryUp[i][0] = '\0';
          }
        }
        strcpy(entryUp[up], entry[index]);
        up++;
      }
      else if (entry[index][0] == '~'){
        entryCu[cu] = (char *)malloc(sizeof(char)*strlen(entry[index]));
        for (int i = 0; i < cu; i++){
          if(!strcmp(entry[index], entryCu[i])){
            if(!strcmp(read_vari, "y")) printf("%s -> %s is deleted\n", dir_entry->d_name, entry[index]);
            unlink(dir_entry->d_name);
            entryCu[i][0] = '\0';
          }
        }
        strcpy(entryCu[cu], entry[index]);
        cu++;
      }
      else {
        if(!strcmp(read_vari, "y")) printf("%s -> %s is deleted\n", dir_entry->d_name, entry[index]);
        unlink(dir_entry->d_name);
      }
    }
    index++;
  }
}

int main(int argc, char* argv[])
{
  char *val = malloc(sizeof(char) * 100);
  jmp_buf current_program;
  printf("\nDo you want to show the file to be deleted? (y/n)\n"); 
  setjmp(current_program);
  scanf("%s", val);
  fgetc(stdin);
  if(strcmp("n", val) != 0 && strcmp("y", val) != 0){
    printf("Wrong input! y or n\n");
    longjmp(current_program, 1);
  }
  delHardlink(argv[1], val);
  delSymlink(argv[1], val);
  return 0;
}
